"""
Nose tests for acp_times.py

Here we test if the dates are incremented accuratly according to the RUSA Algorithm 
found at: https://rusa.org/pages/acp-brevet-control-times-calculator
"""
from acp_times import open_time, close_time

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)




def test_open_time_60km():
	openResult = open_time(60.0, 200, "2017-01-01T00:00+00:00")
	assert openResult == "2017-01-01T01:46:00+00:00"

def test_close_time_60km():
	closeResult = close_time(60.0, 200, "2017-01-01T00:00+00:00")
	assert closeResult == "2017-01-01T04:00:00+00:00"

def test_open_time_120km():
	openResult = open_time(120.0, 200, "2017-01-01T00:00+00:00")
	assert openResult == "2017-01-01T03:32:00+00:00"

def test_close_time_120km():
	closeResult = close_time(120.0, 200, "2017-01-01T00:00+00:00")
	assert closeResult == "2017-01-01T08:00:00+00:00"

def test_longRace():
	openResult = open_time(300.0, 300, "2017-01-01T00:00+00:00")
	assert openResult == "2017-01-01T09:22:00+00:00"

def test_raceError():
	"""test case to show acp returns 0 when an error occurs """
	openResult = open_time(400.0, 300, "2017-01-01T00:00+00:00")
	assert openResult == 0





"""
def test_jumbled_single():
    assert same(jumbled(["abbcd"], 1), "abbcd")


def test_jumbled_pair():
    assert same(jumbled(["abbc", "abcc"], 2), "abbcc")


def test_jumbled_more():
    assert same(jumbled(["aabc", "abac", "bcaa"], 2), "aabc")

if __name__ == "__main__":
	test_open_time_60km()
	test_close_time_60km()
	test_open_time_120km()
	test_close_time_120km()
	test_longRace()
	test_raceError()
"""
