"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

			#[(0-200), (200-400), (400-600), (600-1000), (1000-1300)]
		   #(min, max)
brevTable = [(15,34), (15, 32), (15, 30), (11.428, 28), (13.333, 26)]


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    #time start

    #create date time object from the brev_start_time
    #"2013-05-05T12:30:45+00:00"
    if (control_dist_km > brevet_dist_km):
    	#print("control distance longer than brevet, now exiting")
    	return 0

    dt = brevet_start_time.split("T")

    date = dt[0].split("-")
    zone = dt[1].split("+")[1]
    time = dt[1].split("+")[0]
    time = time.split(":")

    """
    print(date)
    print(time)
    print(zone)
    """
    
    if (len(time) == 3):
    	start = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    else:
    	start = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]))


    #print(start)



    #calculate the time to add based on the control_dist_km
    if (control_dist_km <= 200):
    	shiftHour = control_dist_km/brevTable[0][1]
    elif ((control_dist_km > 200) and (control_dist_km <= 400)):
    	shiftHour = control_dist_km/brevTable[1][1]
    elif ((control_dist_km > 400) and (control_dist_km <= 600)):
    	shiftHour = control_dist_km/brevTable[2][1]
    elif ((control_dist_km > 600) and (control_dist_km <= 1000)):
    	shiftHour = control_dist_km/brevTable[3][1]
    elif ((control_dist_km > 1000) and (control_dist_km <= 1300)):
    	shiftHour = control_dist_km/brevTable[4][1]
    else:
    	#how to handle the execption?
    	#print("error, invalid km entered, now exiting")
    	return 0

    minutes = round(float(str(shiftHour-int(shiftHour))[1:]) * 60)
    shiftHour = shiftHour // 1
    #print(shiftHour)
    #print(minutes)



    #shift the start object
    start = start.shift(hours = +shiftHour)
    start = start.shift(minutes = +minutes)
    #print("open_time: ",start)

    #start = start.shift(hours = +8)
    #print("start time: ", start)
    #date_time = start.format('YYYY-MM-DD HH:mm:ss')
    #print("Date and time: {0}".format(date_time))


    return start.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if (control_dist_km > brevet_dist_km):
    	#print("control distance longer than brevet, now exiting")
    	return 0

    dt = brevet_start_time.split("T")

    date = dt[0].split("-")
    zone = dt[1].split("+")[1]
    time = dt[1].split("+")[0]
    time = time.split(":")

    """
    print(date)
    print(time)
    print(zone)
    """
    

    if (len(time) == 3):
    	start = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    else:
    	start = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]))
    #print(start)



    #calculate the time to add based on the control_dist_km
    if (control_dist_km <= 200):
    	shiftHour = control_dist_km/brevTable[0][0]
    elif ((control_dist_km > 200) and (control_dist_km <= 400)):
    	shiftHour = control_dist_km/brevTable[1][0]
    elif ((control_dist_km > 400) and (control_dist_km <= 600)):
    	shiftHour = control_dist_km/brevTable[2][0]
    elif ((control_dist_km > 600) and (control_dist_km <= 1000)):
    	shiftHour = control_dist_km/brevTable[3][0]
    elif ((control_dist_km > 1000) and (control_dist_km <= 1300)):
    	shiftHour = control_dist_km/brevTable[4][0]
    else:
    	#how to handle the execption?
    	#print("error, invalid km entered, now exiting")
    	return 0

    minutes = round(float(str(shiftHour-int(shiftHour))[1:]) * 60)
    shiftHour = shiftHour // 1
    #print(shiftHour)
    #print(minutes)



    #shift the start object
    start = start.shift(hours = +shiftHour)
    start = start.shift(minutes = +minutes)



    return start.isoformat()


"""
def main():
	open_time(60.0, 200, "2017-01-01T00:00+00:00")
	close_time(200, 200, "2017-01-01T00:00+00:00")

if __name__ == "__main__":
	main()
"""
