"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
from pymongo import MongoClient
import pprint
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

#logging.basicConfig(format='%(levelname)s:%(message)2',  level=logging.INFO) 
#log = logging.getLogger(__name__)
app.logger.setLevel(logging.DEBUG)

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############




@app.route('/db')
def new():
    #parse input string
    timesString = request.args.get('timesString');
    timeArray = timesString.split("x");
    app.logger.debug("timeArray={}".format(timeArray))

    #clear database
    db.tododb.drop()


    half = (len(timeArray)-1)
    app.logger.debug("half={}".format(half))
    for i in range(0, len(timeArray)-1, 2):
        #app.logger.debug("timesArray[i]={}".format(timeArray[i]));
        #app.logger.debug("timesArray[i+1]={}".format(timeArray[i+1]));
        item_doc = {'open_time': timeArray[i], 'close_time':timeArray[i+1]}
        db.tododb.insert_one(item_doc);


    
    for obj in db.tododb.find():
        print(obj['open_time'])
        #app.logger.debug("open_time={}, close_time={}".format(obj['open_time'], obj['close_time']))
    

    

    result = {"success": "success"}
    return flask.jsonify(result=result)



@app.route("/_display")
def dis():

    _items = db.tododb.find()
    items = [item for item in _items]

    if (len(items) == 0):
        
        return render_template('subError.html');


    for item in items:
        app.logger.debug("open_time11={}, close_time={}".format(item['open_time'], item['close_time']))

    return render_template('todo.html', items=items)




@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    trueDistance = request.args.get('trueDistance',type=int)
    app.logger.debug("trueDistance={}".format(trueDistance))

    startTime = request.args.get('startTime',type=str)
    date = request.args.get('date',type=str)
    app.logger.debug("startTime={}".format(startTime))
    app.logger.debug("date={}".format(date))

     #"2013-05-05T12:30:45+00:00"
    inputStr = date + "T" + startTime + "+" + "00:00"
    #print("inputStr: ",inputStr)
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    

    # I changed the code here to shift the time by 8 hours because without it 
    # moment.js would misformat the string. This bug was encountered on line 149,150 in calc.html
    open_time = acp_times.open_time(km, trueDistance, inputStr)

    if (open_time == 0):
        error = "error"
        result = {"error": error}
        return flask.jsonify(result = result)
    
    dt = open_time.split("T")

    date = dt[0].split("-")
    zone = dt[1].split("+")[1]
    time = dt[1].split("+")[0]
    time = time.split(":")

    start_time2 = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    start_time2 = start_time2.shift(hours = +8)
    open_time = start_time2.isoformat()

    close_time = acp_times.close_time(km, trueDistance, inputStr)
    dt = close_time.split("T")
    date = dt[0].split("-")
    zone = dt[1].split("+")[1]
    time = dt[1].split("+")[0]
    time = time.split(":")

    close_time2 = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    close_time2 = close_time2.shift(hours = +8)
    close_time = close_time2.isoformat()


    result = {"open": open_time, "close": close_time}
    
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
