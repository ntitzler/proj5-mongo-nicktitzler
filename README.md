# Project 5: Brevet time calculator with Ajax and mongodb

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.
Author of acp_times.py and implementation of flask_brevets.py by Nick Titzler, ntitzler@uoregon.edu

## Overview

This calculator gives thes the start and close times for a randonneuring race. This is a type of cycling race where riders aim to keep
a given pace, and arrive at control checkpoints within the designated time limits. For more information on the sport vist: https://rusa.org/

## The algorithm for calculating brevet control times

From the offical RUSA website:

control location		min speed		max speed
	0-200					15				34
	200-400					15				32
	400-600					15				30
	600-1000				11.428			28
	1000-1300				13.333			26

 "The calculation of a control's opening time is based on the maximum speed (see above chart). Calculation of a control's closing time is based on the minimum speed. When a distance in kilometers is divided by a speed in kilometers per hour, the result is a time measured in hours. For example, a distance of 100 km divided by a speed of 15 km per hour results in a time of 6.666666... hours. To convert that figure into hours and minutes, subtract the whole number of hours (6) and multiply the resulting fractional part by 60. The result is 6 hours, 40 minutes, expressed here as 6H40.

The calculator converts all inputs expressed in units of miles to kilometers and rounds the result to the nearest kilometer before being used in calculations. Times are rounded to the nearest minute."

further explanation can be found at: https://rusa.org/pages/acp-brevet-control-times-calculator

This information is necessary to successfully using the calculator

## Calculator use

To use the date calculator, simply input the km or miles distance for each of your control points  


## Explanation of Error handling

If the entered km or miles is longer than that of the total race distance, "error in input" will be printed in the date slot




## For developers: AJAX and Flask implementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

Ajax is used in conjunction with a python flask file, which will collect the km and miles distance and subsequently return the correct open and close times. The flask file, flask_brevets.py collects the arugements, and acp_times.py processes the correct open and close time

* Each time a distance is filled in, the corresponding open and close times are filled in with Ajax.

Nose test cases are provided and can be found in test_acp_times.py   


## Test case development:

	Test1: While the app is running, if the submit button is pressed while the table is empty an alert will be displayed

	Test2: While the app is running, if the display button is pressed and the table has not been populated, a error template is displayed
	prompting the user to return to the index page

